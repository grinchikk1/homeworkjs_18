function cloneObj(obj) {
  if (obj === null || typeof obj !== "object") {
    return obj;
  }

  const clone = Array.isArray(obj) ? [] : {};

  for (const key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      clone[key] = cloneObj(obj[key]);
    }
  }

  return clone;
}

const object = {
  name: "Dimas",
  age: 33,
  hobbies: ["reading", "playing"],
  address: {
    city: "Kyiv",
    country: "UA",
  },
};

const objectClon = cloneObj(object);

console.log(objectClon);
